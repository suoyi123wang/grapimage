package com.camille.grapimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class GrapimageApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrapimageApplication.class, args);
    }

}
