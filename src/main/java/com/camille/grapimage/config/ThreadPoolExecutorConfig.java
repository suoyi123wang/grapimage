package com.camille.grapimage.config;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.ThreadFactory;

@Configuration
public class ThreadPoolExecutorConfig {
    @Value("20")
    private Integer corePoolSize;
    @Value("200")
    private Integer keepAliveTime;
    @Value("40")
    private Integer maxPoolSize;
    @Value("20")
    private Integer queueSize;

    @Bean("threadPoolTaskScheduler")
    public ThreadPoolTaskScheduler threadPoolTaskScheduler(){
        ThreadFactory threadFactory = new ThreadFactoryBuilder().setNameFormat("download_image_from_bing").build();
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.setThreadFactory(threadFactory);
        threadPoolTaskScheduler.setPoolSize(corePoolSize);
        return threadPoolTaskScheduler;
    }
}
