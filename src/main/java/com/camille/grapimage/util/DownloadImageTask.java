package com.camille.grapimage.util;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
@Scope(scopeName = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class DownloadImageTask implements Runnable{

    public void downloadImg(String url,String path,String fileName){
        try {
            URL realUrl = new URL(url);
            HttpURLConnection urlConnection = (HttpURLConnection)realUrl.openConnection();
            InputStream inputStream = urlConnection.getInputStream();
            byte[] bytes = new byte[1024];
            FileOutputStream fileOutputStream = new FileOutputStream(path+fileName);
            int len = inputStream.read(bytes);
            while (len>0){
                fileOutputStream.write(bytes,0,len);
                len = inputStream.read(bytes);
            }
            inputStream.close();
            fileOutputStream.close();
            urlConnection.disconnect();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void run(){
        System.err.println("下载图片");
        String url = "https://cn.bing.com/";
        ParseUrl grabImgFromBing = new ParseUrl();
        grabImgFromBing.getHrefUrl(url);
        downloadImg(url+new ParseUrl().getHrefUrl(url),
                "/Users/miaomiaowang/Desktop/北交/bing_pic/",
                new SimpleDateFormat("yyyy-MM-dd").format(new Date())+".jpeg");
    }
}
