package com.camille.grapimage.util;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseUrl {


    /**
     * 获取每日图片的下载链接，还需要再拼接上"https://cn.bing.com/" 才可
     * @param url bing网页地址
     * @return
     */
    public String getHrefUrl(String url){
        try {
            URL realUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) realUrl.openConnection();
            // 设置连接超时
            connection.setConnectTimeout(10*1000);

            InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream(),
                    getCharset(connection.getContentType()));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String readLine = bufferedReader.readLine();
            while (!StringUtils.isEmpty(readLine)){
                if (!StringUtils.isEmpty(getHref(readLine)))
                    return getHref(readLine);
                readLine = bufferedReader.readLine();
            }
            connection.disconnect();

        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 获取网页的编码方式
     * @param str ？？？
     * @return
     */
    private String getCharset(String str){
        Pattern pattern = Pattern.compile("charset=.*");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find())
            return matcher.group(0).split("charset=")[1];
        return null;
    }

    /**
     * 获取一行中有link标签的链接
     * 这里只针对bing上的每日图片，所以特殊化了，
     * @param str 读取的一行内容
     * @return
     */
    private String getHref(String str){
        Pattern pattern = Pattern.compile("<link .*>");
        Matcher matcher = pattern.matcher(str);
        if (matcher.find())
            return matcher.group().split("\"")[5];
        return null;
    }

}
