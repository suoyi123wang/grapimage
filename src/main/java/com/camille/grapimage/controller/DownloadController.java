package com.camille.grapimage.controller;

import com.camille.grapimage.service.ExecuteService;
import com.camille.grapimage.util.DownloadImageTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/camille/download")
@RestController
public class DownloadController {
    @Autowired
    private ExecuteService executeService;

    @RequestMapping("/bing")
    public void download(){
        System.err.println("正在执行");
//        executeService.startDownload();
        new Thread(
                new DownloadImageTask()).start();
    }
}
