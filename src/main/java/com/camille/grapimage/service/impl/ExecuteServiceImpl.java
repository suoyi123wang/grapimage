package com.camille.grapimage.service.impl;

import com.camille.grapimage.util.DownloadImageTask;
import com.camille.grapimage.service.ExecuteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.TimeZone;
import java.util.concurrent.ScheduledFuture;

@Component
@Service
public class ExecuteServiceImpl implements ExecuteService {
    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;

    @Override
    public void startDownload(){
        System.err.println("执行任务");
        Trigger trigger = new CronTrigger("0 15 16 1/1 * ?", TimeZone.getDefault());
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(DownloadImageTask.class);
        enhancer.setCallback((MethodInterceptor)(o, method, objects, methodProxy)->methodProxy.invokeSuper(o,objects));
        DownloadImageTask runnable = (DownloadImageTask)enhancer.create();
        ScheduledFuture<?> schedule = threadPoolTaskScheduler.schedule(runnable, trigger);
    }
}
