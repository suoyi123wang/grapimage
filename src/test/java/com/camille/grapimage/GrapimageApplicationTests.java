package com.camille.grapimage;

import com.camille.grapimage.service.ExecuteService;
import com.camille.grapimage.service.impl.ExecuteServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootTest
@EnableScheduling
class GrapimageApplicationTests {
    @Autowired
    private ExecuteService executeService;

    @Test
    void contextLoads() {
        executeService.startDownload();
    }

}
